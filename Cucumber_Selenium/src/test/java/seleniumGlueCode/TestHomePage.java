package seleniumGlueCode;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class TestHomePage {
	 public static WebDriver driver;
	 
	 @Given ("^I am in the home page$")
	 public void user_is_on_homepage() throws Throwable {     
		 System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverMac");
	        driver = new ChromeDriver();
	        //Maximize Window
	        driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        driver.get("http://www.google.com");
	 }
	
	/* @Given("^user launch googlee$")
	    public void user_is_on_homepage() throws Throwable {     
		 System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverMac");
	        driver = new ChromeDriver();
	        //Maximize Window
	        driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        driver.get("http://www.google.com");
	        driver.close();
	        driver.quit();
	    }*/
	 @When("^I find the search box and send keys$") 
	   public void input_Text_In_Search_Box() throws Throwable{
		 WebElement searchBox = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div/div[1]/div/div[1]/input"));
	    	try {
				Thread.sleep(2000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	if(searchBox.isEnabled()) {
	    		searchBox.sendKeys("Mozilla");
	    	}	
	 }
	 @When("^I click on the type ahead link$" )
	 	public void clickTypeAhead() throws Throwable{
		 try {
			    //	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			    	
					Thread.sleep(4000);
			    	//*[@id="tsf"]/div[2]/div/div[2]/div[2]/ul/li[3]/div[1]/div/span/b
			    	WebElement typeAhead = driver.findElement(By.xpath( "//span[.='mozilla']")) ;
			    	System.out.println("found TypeAheand");
			    	//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		            Thread.sleep(4000);
			    	if(typeAhead.isDisplayed() && typeAhead.isEnabled()) {
			    		typeAhead.click();
			    	}
			    	Thread.sleep(4000);
			    	WebElement downloadFirefoxLink =  driver.findElement(By.xpath( "//*[@id=\'rso\']/div[1]/div/div/table/tbody/tr[1]/td[1]/div/span/h3/a")) ;
			    	System.out.println("found downloadFirefoxLink");
			    	if(downloadFirefoxLink.isDisplayed() && downloadFirefoxLink.isEnabled()) {
			    		downloadFirefoxLink.click();
			    	}
			    	} catch(Exception ex) {
			    		ex.printStackTrace();
			    	}
	
}
	 @Then("^I should be in new page$")
	 public void verify_if_in_new_page() throws Throwable{
		 try {
			 String title = driver.getTitle();
		    	Assert.assertEquals(title, "Download Firefox — Free Web Browser — Mozilla");
		    	driver.close();
		    	        driver.quit();
		    	  
			 
		 }catch (Exception ex) {
			 ex.printStackTrace();
			 
		 }
	 }
}
