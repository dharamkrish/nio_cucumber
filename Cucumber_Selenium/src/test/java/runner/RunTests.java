package runner;
import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;



import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@CucumberOptions(
features = "src/test/java/test/feature"
,glue= {"seleniumgluecode"}
)

@RunWith(Cucumber.class)
public class RunTests {

}
